---
title: 【笔记】JavaScript学习笔记
date: 2021-9-15 16:58:42
tags: [笔记,JavaScript]
---



JavaScript的学习笔记，基于狂神的视频总结的。

[原视频](https://www.bilibili.com/video/BV1JJ41177di?spm_id_from=333.999.0.0)

<!-- more -->

---



#  1、什么是JavaScript？

## 1.1、概述

**JavaScript是一门世界上最流行的脚本语言**

java和JavaScript没有关系

JavaScript只用了10天就开发出来

**一个合格的后端人员，必须要精通JavaScript**



## 1.2、历史

   网景公司的整个管理层，都是Java语言的信徒，Sun公司完全介入网页脚本语言的决策。因此，Javascript后来就是网景和Sun两家公司一起携手推向市场的，这种语言被命名为"Java+script"并不是偶然的。

此时，34岁的系统程序员Brendan Eich登场了。1995年4月，网景公司录用了他。

Brendan Eich的主要方向和兴趣是函数式编程，网景公司招聘他的目的，是研究将Scheme语言作为网页脚本语言的可能性。Brendan Eich本人也是这样想的，以为进入新公司后，会主要与Scheme语言打交道。

仅仅一个月之后，1995年5月，网景公司做出决策，未来的网页脚本语言必须"看上去与Java足够相似"，但是比Java简单，使得非专业的网页作者也能很快上手。这个决策实际上将Perl、Python、Tcl、Scheme等非面向对象编程的语言都排除在外了。

Brendan Eich被指定为这种"简化版Java语言"的设计师。

但是，他对Java一点兴趣也没有。为了应付公司安排的任务，他只用10天时间就把Javascript设计出来了。

由于设计时间太短，语言的一些细节考虑得不够严谨，导致后来很长一段时间，Javascript写出来的程序混乱不堪。如果Brendan Eich预见到，未来这种语言会成为互联网第一大语言，全世界有几百万学习者，他会不会多花一点时间呢？

总的来说，他的设计思路是这样的：

> 　　　　（1）借鉴C语言的基本语法；
>
> 　　　　（2）借鉴Java语言的数据类型和内存管理；
>
> 　　　　（3）借鉴Scheme语言，将函数提升到"第一等公民"（first class）的地位；
>
> 　　　　（4）借鉴[Self语言](http://en.wikipedia.org/wiki/Self_(programming_language))，使用基于原型（prototype）的继承机制。

所以，Javascript语言实际上是两种语言风格的混合产物----（简化的）函数式编程+（简化的）面向对象编程。这是由Brendan Eich（函数式编程）与网景公司（面向对象编程）共同决定的。

> 这篇博客详细的讲述了JavaScript的起源https://blog.csdn.net/kese7952/article/details/79357868，感兴趣的铁汁可以去看看。



**ECMAScript它可以理解为JavaScript的一个标准**   最新版本已经到es6版本，但是大部分浏览器还只停留在支持es5代码上！

导致**开发环境和线上环境**，版本不一致。



# 2、快速入门

## 2.1、引入JavaScript



1. 内部标签引入

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
   <!--    内部标签引入-->
   <!--  script标签内写 JavaScript代码-->
       <script>
           alert('hellow world');
       </script>
   </head>
   <body>
   
   </body>
   </html>
   ```

2. 外部引入

   abs.js

   ```javascript
   ///
   ```

   test.html

   ```html
   <script src="abs.js"></script>
   ```

   

   ## 2.2、基本语法入门

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
     <!--JavaScript严格区分大小写-->
     <script>
       // 1.定义变量  变量名=变量值;
       var score=71;
       // 2.条件控制
       if(score>60&&score<70) {
         alert("60~70")
       }else if (score>70&&score<80){
         alert("70~80")
       }else{
         alert("other")
       }
   
       //console.log(score)  在浏览器控制台打印变量   类似于 ststem.out.println();
     </script>
   </head>
   <body>
   
   
   
   </body>
   </html>
   ```

   

![image-20210807200349653](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210807200349653.png)

## 2.3、数据类型

**==变量==**

```javascript
var 
```

JavaScript中变量的定义使用var   

==**number**==

js不区分小数和整数，number

```javascript
123  //整数123
123.1  //浮点数123.1
1.123e3 //科学计数法
-99  //负数
NaN  //not a number
Infinity  //表示无限大
```

**==字符串==**

‘abc’

“abc”

==**布尔值**==

true，false

==**逻辑运算**==

&&

||

！

==**比较运算符**==

```
=  赋值
==  等于（类型不一样，值一样也会判断为true）
例如：1==“1”
===  绝对等于（类型一样，值一样，才为true）
```

这是js的一个缺陷，坚持不要使用 == 比较

**须知**

- NaN=NaN，这个与所有的数值都不相等，包括它本身
- **只能通过isNaN(NaN)来判断这个数是否是NaN**



**==浮点数问题：==**

```javascript
console.log(1/3===(1-2/3))
//结果为 false  浮点数运算会发生精度丢失
```

尽量避免使用浮点数进行运算，存在精度问题

```
Math.abs(1/3-(1-2/3))<0.00000001
```



**==null和undefined==**

- null  空
- undefined 未定义



**==数组==**

java的数组必须是类型相同的对象，而JavaScript中则不需要这样

```javascript
//保证代码的可读性，尽量使用[]
var arr=[1,2,3,4,5,'hello',null,ture];

new Array(1,12,3,4,5,'hello');
```

取数组下标如果下标越界，就会打印**undefined**



**==对象==**

对象使用大括号，数组是中括号

**每个属性之间使用逗号隔开，最后一个属性不需要添加逗号**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
  <script>
    var Person;
    Person = {
      name: "likaibin",
      age: 20,
      tags: ['js', 'c++', 'python']
    };
  </script>
</head>
<body>

</body>
</html>
```

**取对象的值：**

```
Person.name
"likaibin"
```

  

## 2.4、严格检查模式

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script>
        // "use strict";  严格检查模式，预防JavaScript的随意性导致的一些问题
        //  前提 idea需要设置支持ES6语法
        //  且必须写在JavaScript的第一行
        "use strict";
        i=1;
        // ES6 局部变量使用 let定义

    </script>
</head>
<body>

</body>
</html>
```

直接令i=1，JavaScript会把i当成全局变量，在严格检查模式中会报错，若不开启严格检查模式则不会报错。

![image-20210808104001502](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808104001502.png)

设立"严格模式"的优点：

1. 消除Javascript语法的一些不合理、不严谨之处，减少一些怪异行为;

2. 消除代码运行的一些不安全之处，保证代码运行的安全；

3. 提高编译器效率，增加运行速度；

4. 为未来新版本的Javascript做好铺垫。

注：经过测试 IE6,7,8,9 均不支持严格模式。





# 3、数据类型

## 3.1、字符串

1. 正常字符串我们使用 单引号和双引号包裹
2. 注意转义字符   \

```
\'  代表这是一个字符串
\n  换行
\t   是tab
\u4e2d  \u####  unicode字符
\x41    ascll字符
```

3. 多行字符串编写

   ``  长字符串（在tab键上方）     包裹的可以写多行

   ```javascript
   var msg=`
             hello
             world
              你好世界`
   
   ```

4. 模板字符串 

   ```javascript
    let name= 'likaibin';
       let age= 18;
       let mag=`你好呀，${name}`   //此处的``为tab键上面的
       alert(mag);
   ```

   

   ![image-20210808105632516](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808105632516.png)

5. 字符串长度

   ```javascript
   var student="student";
   alert(student.length);
   alert(student[0]);
   ```

   ![image-20210808110100092](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808110100092.png)

   ![image-20210808110254252](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808110254252.png)

6. 字符串不可变性

7. 大小写转换

   ```javascript
   // 注意，这是方法不是属性
   student.toUpperCase()
   
   student.toLowerCase()
   ```

   

## 3.2、数组

**Array可以包含任意的数据类型**

```javascript
var arr = [1,2,3,4,5,6]
```

1. 长度

   ```javascript
     var arr =[1,2,3,4,5,6]
       arr.length=10;
       console.log(arr)
       console.log(arr.length)
   ```

   注意：给arr.length赋值，数组大小会发生改变

   ![image-20210808150218245](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808150218245.png)

2. indexOf，通过元素获得下标索引

   ![image-20210808150633905](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808150633905.png)

​          **字符串“1”和数字 1 是不同的**



3. slice（）截取Array的一部分，返回一个新的数组，类似于string中的substring

   ![image-20210808151032393](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808151032393.png)

​         从下标为2的元素开始往后截取，**前闭后开**原则。



4. push和pop  尾部

   ``` 
   push()  压入到尾部
   pop()   弹出尾部的一个元素
   ```

   

   ![image-20210808151430627](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808151430627.png)

   

5. unshift() 和shift() 头部

   ```
   unshift():  压入到头部
   shift():  弹出头部的一个元素
   ```

   

6. 排序sort（）

   ![image-20210808151943289](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808151943289.png)

7. 元素反转 reverse（）

   ![image-20210808152107193](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808152107193.png)

8. 数组拼接concat（）

   ![image-20210808152329890](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808152329890.png)

​         **注意：concat（）并不会改变原数组而是会产生一个新的数组**

9. 连接符join（）

   打印拼接数组，使用特定的符号连接

   ![image-20210808152650232](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808152650232.png)

10. 多维数组

    ![image-20210808153018737](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210808153018737.png)

 

## 3.3、对象

若干个键值对描述属性，多个属性之间使用逗号隔开，最后一个属性不加逗号。

```javascript
var person = {
    属性名:属性值，
    属性名:属性值，
    属性名:属性值
}
var person = {
    name:"likaibin",
    age:18,
    score:100
}
```

**JavaScript中所有的键都是字符串，值是任意对象。**

1. 对象赋值

   ```javascript
   person.name
   "likaibin"
   person.name="LiKaiBin"
   "LiKaiBin"
   person.name
   "LiKaiBin"
   ```

2. 使用一个不存在的对象属性，不会报错！undefined

3. 动态删减属性，通过delete删除对象的属性

   ```javascript
   delete person.name
   true
   person
   {age: 18, score: 100}
   ```

4. 动态的添加，直接给新的属性添加值即可

   ```javascript
   person.sex = "man"
   "man"
   person
   {age: 18, score: 100, sex: "man"}
   ```

5. 判断属性值是否在这个对象中  xxx in xxx

   ```javascript
   person
   {age: 18, score: 100, sex: "man"}
   "age" in person
   true
   "name" in person
   false
   
   ```

6. 判断一个属性是否是这个对象自身拥有的  使用hasOwnPropert（）

   ```javascript
   person.hasOwnProperty("toString")
   false
   person.hasOwnProperty("age")
   true
   ```

   

## 3.4、流程控制

if判断

```javascript
var age=10;
if (age>10){
    alert("xixixi");
}else if(age>20){
    alert("hhh");
}else{
    alert("kuwa");
}
```

while循环，避免程序死循环

```javascript
while(age<100){
    age=age + 1;
    console.log(age) 
} 
```

for循环

```javascript
for(let i = 0;i < 100; i++){
    console.log(i)
}
```



## 3.5、Map和Set

Map:

```javascript
//  ES6中才有
        var map = new Map(["tom",100],["jack",99]);
        var name = Map.get("tom");   //通过key  获得value
        Map.set("admin",88);
        console.log(Map);
        Map.delete("tom");
```

Set：无序不重复集合

```javascript
var set = new Set[3,1,1,1]  //set可以去重
        set.add(2);   //添加
        set.delete(1);  //删除
        console.log(set.has(3));  //是否包含某个元素
```



# 4、函数

## 4.1、定义函数

> 定义方式一：

绝对值函数

```javascript
        function abs(x) {

            if(x >= 0) {
                return x;
            }else {
                return -x;
            }
        }

        console.log(abs(-9));

```

一旦执行到 return 代表函数结束,返回结果！

**在 JavaScript 中,如果没有执行 return，函数执行完也会返回结果,结果就是 NaN,undefined。**

> **定义方式二：**

```javascript
let abs = function (x) {
         if(x >= 0) {
             return x;
          }else {
             return -x;
          }
      };
  console.log(abs(-90));
```

function(x){…} 是一个匿名函数,但是可以把结果赋值给 abs,通过 abs 就可以调用函数。

方式一和方式二等价！

> 调用函数

```javascript
abs(10)  //10
abs(-10)  //-10
```

参数问题：javaScript 可以传递任意个参数，也可以不传递参数~
 参数进来是否存在的问题？
 假设不存在参数，如何规避？

```javascript
var abs = function (x){
    //手动抛出异常来判断
    if(typeof x !== 'number'){
      throw 'Not a Number';
    }
    if(x >= 0){
      return x;
    } else {
      return -x;
    }
  }
```

> arguments

`arguments`是一个JS免费赠送的关键字；
 代表，传递进来的所有的参数，是一个数组！

```javascript
  var abs = function (x){
    console.log("x=>"+x);

    for(let i = 0; i< arguments.length;i++){
      console.log(arguments[i]);
    }

    if(x >= 0){
      return x;
    } else {
      return -x;
    }
  }
```

问题：arguments包含所有的参数，我们有时候想使用多余的参数进行附加操作，需要排除已有的参数~

> rest

以前：

```javascript
if(arguments.length > 2){
	//....
}
```

ES6 引入的新特性，获取除了已经定义的参数之外的所有参数~

```javascript
function aaa(a,b,...rest){
        console.log("a=>"+a);
        console.log("b=>"+b);
        console.log(rest);
    }
```

**rest 参数只能写在最后面，必须用…标识**

## 4.2、变量的作用域

在javascript中，var定义变量实际是有作用域的。
 假设在函数体中声明，则在函数体外不可以使用~

```javascript
  function qj(){
        var x = 1;
        x = x + 1;
    }
    x = x + 2; //Uncaught ReferenceError: x is not defined
```

如果两个函数使用了相同的变量名，只要在函数内部，就不冲突

```javascript
function qj(){
    var x = 1;
    x = x + 1;
}

function qj2(){
    var x = 'A';
    x = x + 1;
}
```

内部函数可以访问外部函数的成员，反之则不行

```javascript
function qj(){
    var x = 1;

    //内部函数可以访问外部函数的成员，反之则不行
    function qj2(){
        var y = x + 1;  //2
    }

    var z = y + 1; //Uncaught ReferenceError: y is not defined
}
```

假设，内部函数变量和外部函数的变量，重名！

```javascript
function qj(){
    var x = 1;
    function qj2(){
        var x = 'A';
        console.log('inner'+x); //innerA
    }
    qj2();
    console.log('outer'+x); //outer1

}
qj();
```

假设在JavaScript 中 函数查找变量从自身函数开始~，由’内’ 向 '外’查找，假设外部存在这个同名的函数变量，则内部函数会屏蔽外部函数的变量。

> 提升变量的作用域

```javascript
function qj(){
        var x = "x" + y;
        console.log(x);
        var y = "y";
    }

```

结果： xundefined
 说明： js执行引擎，自动提升了y的声明，但是不会提升变量y的赋值；

```javascript
function qj2(){
		var y;

        var x = "x" + y;
        console.log(x);
        var y = "y";
    }
```

这个是JavaScript建立之初就存在的特性，养成规范；所有的变量定义都放在函数的头部，不要乱放，便于代码维护；

```javascript
function qj2(){
		var x = 1,
			y = x + 1,
			z,i,a; // undefined
			
		//之后随意用
    }
```

> 全局函数

```javascript
  //全局变量
    x = 1;

    function f(){
        console.log(x);
    }
    f();
    console.log(x);
```

全局对象**window**

```javascript
var x = 'xxx';
alert(x);
alert(window.x);//默认所有的全局变量，都会自动绑定在window对象下;
```

alert()这个函数本身也是一个window的变量；

```javascript
var x = 'xxx';

window.alert(x);

var old_alert = window.alert;

//old_alert(x);

window.alert = function(){

};
//发现 alert() 失效了
window.alert(123);

//恢复
window.alert = old_alert;
window.alert(456);
```

javascript实际上只有一个全局作用域，任何变量（函数也可以视为变量），假设没有在函数作用范围内找到，就会向外查找，如果在全局作用域都没有找到，就会报错`RefrenceError`

> 规范

由于我们所以的全局变量都会绑定到window 上，如果不同的js文件，使用李相同的全局变量，如何使冲突减少?

```javascript
//唯一全局变量
var ALL = {};

//定义全局变量
ALL.name = "likaibin";
All.add = function(a,b){
    return a + b;
}
```

把自己的代码全部放入自己定义的唯一空间名字中，降低全局命名冲突的问题。

> 局部作用域let

```javascript
function aaa(){
    for(var i = 0; i<100;i++){
        console.log(i)
    }
    console.log(i+1);
    //问题   i出了for循环的作用域还可与使用
}
```

![image-20210810161719112](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810161719112.png)

ES6 let关键字，解决局部作用域冲突的问题

```javascript
function aaa(){
    for(let i = 0; i<100;i++){
        console.log(i)
    }
    console.log(i+1);
```



![image-20210810161820097](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810161820097.png)

建议大家都使用 `let`去定义局部作用域

> 常量 const

在ES6之前，怎么定义常量，只要用全部大写字母命名的变量就是常量，建议不要修改这样的值

```javascript
var PI = '3.14';
console.log(PI);
PI = '333';   //可以改变这个值
console.log(PI);
```

在ES6引入才常量关键字

```javascript
const PI = '3.14';
console.log(PI);
PI = '123';
```

![image-20210810162625672](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810162625672.png)

## 4.3、方法

> 定义方法

- 就是把函数放在对象的里面，对象里只有两个东西：属性和方法

```javascript
var zhangsan = {
    name : '张三',
    birth : 2000,
    //方法
    age : function () {
        let now = new Date().getFullYear();
        return now - this.birth;
    }
}
zhangsan.age();
//属性
zhangsan.name
//方法，一定要带（）
zhangsan.age()
```

this.代表着什么？拆开上面的代码看看

```javascript
function getAge() {
    let now = new Date().getFullYear();
    return now - this.birth;
}

var zhangsan = {
    name : '张三',
    birth : 2000,
    age : getAge()
}
//zhangsan.age() 可以·
//getAge()  NaN window
//  直接调用getAge() 会报错 因为this指向调用它的对象，此时它找不到this.birth的值
```

this是无法指向的，是默认指向调用它的那个对象

> apply

在JavaScript中可以使用apply控制this指向

```javascript
function getAge() {
    var now = new Date().getFullYear();
    return now - this.birth;
}

var zhangsan = {
    name : '张三',
    birth : 2000,
    age : getAge
}
zhangsan.age();

getAge.apply(zhangsan,[]); //this指向了zhangsan，参数为空
```

# 5、对象

- typeof返回结果

| 类型       | 返回值  | 类型                   | 返回值    |
| :--------- | :------ | :--------------------- | :-------- |
| 数字类型   | number  | null                   | object    |
| 字符串类型 | string  | undefined              | unfefined |
| 布尔类型   | boolean | 函数                   | function  |
| 数组       | object  | 核心对象               | function  |
| 对象       | object  | 浏览器对象模型中的方法 | object    |

## 5.1、Date

- **常用方法**

```javascript
var data = new Date();
data.getFullYear();//年份
data.getMonth();//月份 0~11
data.getDate();//天
data.getDay();//星期几
data.getHours();//小时
data.getMinutes();//分钟
data.getSeconds();//秒
console.log(data = new Date(1616589902676));//转为标准时间
```

- **转换**

```javascript
Wed Mar 24 2021 20:45:02 GMT+0800 (中国标准时间)
now.toLocaleDateString(); //"2021/7/24"
now.toGMTString();  //调用方法
//"Wed, 24 Mar 2021 12:45:02 GMT
```

## 5.2、JSON

> 什么是JSON

早期所有数据传输习惯使用XML文件！

- [JSON](https://baike.baidu.com/item/JSON)([JavaScript](https://baike.baidu.com/item/JavaScript) Object Notation, JS 对象简谱) 是一种**轻量级的数据交换格式。**
- 简洁和清晰的层次结构使得 JSON 成为理想的数据交换语言。
- 易于人阅读和编写，同时也易于机器解析和生成，并有效地提升网络传输效率。

在JavaScript中一切皆为对象，**任何JavaScript支持的类型都可以用JSON表示**；如number,string…

**格式：**

- 对象都用{}
- 数组都用[]
- 所有的键值对都是用 key：value

```javascript
var user = {
    name:'zhangsan',
    age:3,
    sex:'男'
}
//对象转化为json字符串  {"name":"zhangsan","age":3,"sex":"男"}
var jsonUser = JSON.stringify(user);

console.log(jsonUser);
//转换字符串为对象，参数为json字符串
var obj = JSON.parse('{"name":"zhangsan","age":3,"sex":"男"}');
//{name: "zhangsan", age: 3, sex: "男"}
```

**JSON和JavaScript对象的区别**

```javascript
var obj = {a:'hello',b:'hellob'};
var json = '{"a"："hello","b":"hellob"}'
```

## 5.3、Ajax

- 原生的js写法   xhr异步请求
- jQuery封装好的方法  $("#name").ajax("")
- axios请求



# 6、面向对象编程



JavaScript、Java、c#…面向对象编程；JavaScript有何区别

- 类：模板  原形对象
- 对象：具体的实例

在JavaScript这个需要换一下思维方式

原型：

```javascript
var xiaoming = {
    name:"xaioming",
    age : 3,
    run : function () {
        console.log(this.name+"  run....")
    }
}

var xiaozhang = {
    name: "xiaozhang"
}
//原型对象
xiaozhang.__proto__ = xiaoming;

var bird = {
    fly : function () {
        console.log(this.name+"   fly....")
    }
}

//小张的原型是小明
xiaozhang.__proto__ = bird;
function Student(name) {
    this.name = name;
}

//给Student新增一个方法
Student.prototype.hello = function () {
    alert("hello");
};
```

> class继承

**class**关键字，是在ES6引入的

1、定义一个类，属性、方法

```javascript
//定义一个学生的类
class Student{
    constructor(name) {
        this.name = name;
    }
    hello (){
        alert("hello");
    }
}

var xaioming = new Student("xiaoming");
var xaiohong = new Student("xiaohong");
xaioming.hello();
```

2、继承

```javascript
class Student{
    constructor(name) {
        this.name = name;
    }
    hello (){
        alert("hello");
    }
}

class xiaoStudent extends Student{
    constructor(name,grade) {
        super(name);
        this.grade = grade;
    }
    MYGrade(){
        alert("我是一名小学生")
    }
}


var xiaoming = new Student("xiaoming");
var xiaohong = new xiaoStudent("xiaohong,1");
xiaoming.hello();
```

本质：查看对象原型

![image-20210810110358086](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810110358086.png)

> 原型链

```
__ proto __
```

![image-20210810110742241](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810110742241.png)

# 7、操作BOM对象

## 7.1、**浏览器介绍**

JavaScript和浏览器‘之间的关系？

JavaScript诞生就是为了能够在让它在浏览器中运行

BOM：浏览器对象模型

- IE 6-11（Edge）
- Chrome
- Safari
- FireFox  (Linux 使用较多)
- Opera

第三方浏览器：QQ浏览器，360浏览器，Uc浏览器等

## 7.2、**window**

window代表浏览器窗口

```javascript
window.alert("坚持")
undefined
window.innerHeight
760
window.innerWidth
491
window.outerHeight
864
window.outerWidth
1536
//通过window获得浏览器窗口大小
```

## 7.3、Navigator

Navigator,封装了浏览器的信息

```javascript
navigator.appName
"Netscape"
navigator.appVersion
"5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36"
navigator.userAgent
"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36"
navigator.platform
"Win32"
```

大多数时候，我们不会使用navigator对象，因为会被人为修改！

不建议使用这些属性来判断和编写代码

## 7.4、**screen**

代表屏幕尺寸

```javascript
screen.width
1536
screen.height
864
```

## 7.5、location

location代表当前页面的URL信息

```javascript
location.host   "www.baidu.com"  //主机
location.href   "https://www.baidu.com/"   //当前指向的位置
location.protocol   "https:"   //协议
location.reload()   //刷新网页
//设置新的地址
location.assign('https://www.kuangstudy.com/')
```

## 7.6、document

- document代表当前的页面，  HTML  DOM文档树

```javascript
document.title
"百度一下，你就知道"
document.title=('面向监狱编程')
```

- 获取具体的文档树节点

```html
<dl id="app">
    <dt>javaSE</dt>
    <dt>javaEE</dt>
    <dt>javaME</dt>
</dl>

<script>
    var dl =  document.getElementById('app');
</script>
```

- 获取cookie

```javascript
document.cookie
"_uuid=ABB41E69-8A2D-5C9E-91F8-535FE6C5368696306infoc; buvid3=1E000DC4-7833-4DFC-9F80-ACB1E118A758138376infoc; sid=6m3ndo5f; CURRENT_FNVAL=80; blackside_state=1; rpdid=|(um|RYku~um0J'uY||)Y~Rul"
```

- 劫持cookie原理

www.taobao.com

```html
<script src="aa.js"></script>
<!--恶意人员：获取你的cookie上传到他的服务器-->
```

服务器可以设置cookie：httpOnly

## 7.7、history

代表浏览器的历史记录（不建议使用）

```javascript
history.back()//后退
history.forward()//前
```



# 8、操作DOM对象（重点）

## 8.1、**核心**

浏览器网页就是一个Dom树形结构

![image-20210810111155519](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810111155519.png)

- 更新:更新Don节点
- 遍历Dom节点:得到Dom节点
- 删除:删除一个Dom节点
- 添加:添加一个新的的节点

要操作一个Dom节点,就必须先获得这个Dom节点

## 8.2、**获得Dom节点**

```javascript
    var h1 = document.getElementsByTagName("h1");
    var p1 = document.getElementById("p1");
    var p2 = document.getElementsByClassName("p2");
    var father = document.getElementById("father")
    
    var children = father.children;//获取父节点下的所有子节点
    father.lastChild;
    father.firstChild;
```

这是原生代码,之后我们尽量都是用jQurey

## 8.3、更新节点

```javascript
<div id="d1"></div>

<script>
    var d1 = document.getElementById("d1");
</script>
```

操作文本:

- d1.innerText = “123”   修改文本的值
   “123”
- d1.innerHTML = ‘**126**’    可以解析HTML文本标签
   “**126**”

操作css

```javascript
d1.style.color = 'red'  //属性使用 字符串包裹

d1.style.fontFamily = '楷体'  //驼峰命名问题

d1.style.fontSize = "50px"
```

## 8.4、**删除节点**

删除节点的步骤： 先获取父节点，通过父节点删除自己

```html
<div>
    <h1>标题一</h1>
    <p id='p1'>p1</p>
    <p class='p2'>p2</p>
</div>
<script>
    var self = document.getElementByID('p1');
    var father = p1.parentElement; //找到p1的父节点
    father.removechild(self) //通过父节点使用removechild删掉自己（只有标记为id选择器的节点才可以删除)
</script>
```

注意：删除多个节点时，children属性是时刻变化的，删除节点时一定要注意。father.removechild(father.children[0])这样从第一个索引开始删
 ————————————————

## 8.5、插入节点

我们获得了某个Dom节点，假设这个Dom节点是空的，我们会通过innerHTML就可以增加一个元素，但是如果这个Dom节点已经存在元素了，我们就不能这样干了，**会产生覆盖！**

追加操作：

```html
<p id="js">JavaScript</p>
<div id="list">
    <p id="se">JavaSE</p>
    <p id="ee">JavaEE</p>
    <p id="me">JavaME</p>
</div>

<script>
    var js = document.getElementById("js");
    var list = document.getElementById("list")
    list.append(js); //追加节点到后面

</script>
```

![image-20210810111231291](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20210810111231291.png)

> 
> 创建一个新的标签实现插入

```html
<script>
    var list = document.getElementById("list")
    var newP = document.createElement("p"); //创建一个p标签
    newP.id = 'newP';
    newP.innerText = 'sunwukong';
    list.appendChild(newP);

</script>
```

> 创建一个新的标签实现插入

```html
<script>
    var js = document.getElementById("js");//已经存在的节点
    var list = document.getElementById("list")
    list.append(js);
    var newP = document.createElement("p");//创建一个p标签
    newP.id = 'newP';
    newP.innerText = 'sunwukong';
    list.appendChild(newP);

    var myScript = document.createElement("script");//创建一个标签节点
    myScript.setAttribute('type',"text/javascript");
    var body = document.getElementsByTagName("body");
    //body[0].setAttribute('style','backgroundColor = \'#32a356')
    //body[0].style.backgroundColor = '#32a356';

    //创建一个style标签
    var myStyle = document.createElement("style");
    myStyle.setAttribute('type','text/css');
    myStyle.innerText = 'body{background-color:#000000}';//设置标签内容
    document.getElementsByTagName('head')[0].append(myStyle);
</script>
```

> insertBefore

```
<script>
    let ee = document.getElementById('ee');
    let js = document.getElementById('js');
    let list = document.getElementById('list');

    // list: 要包含的节点 (谁在谁前面就把谁放到前面)
    list.insertBefore(js,ee);
/*
JavaSE
JavaScript
JavaEE
JavaME
 */
</script>
```

# 9、操作表单(验证)

## 9.1、表单是什么 form  DOM树

- 文本框 text
- 下拉框 < select >
- 单选框 radio
- 多选框 checkbox
- 隐藏域 hidden
- 密码框 password
- .....

表单的目的：提交信息

> 获得要提交的信息

```html
<form action="#" method="post">
    <p> 
        <span>用户名：</span> <input type="text" id="user">
    </p>

    <p>
        <span>性别：</span>
        <input type="radio" name="sex" value="man" id="boy">男
        <input type="radio" name="sex" value="man" id="girl">女
    </p>

</form>

<script>
    var input_text = document.getElementById("user");
    var boy_redio = document.getElementById('boy');
    var girl_redio = document.getElementById('girl');
    //input_text.value();  得到输出框的值
    input_text.value = '123'  //修改输入框的值

    //对于多选框单选框等固定的值，boy_redio.value只能取到当前的值
    boy_redio.checked;//查看返回的结果是否为true，如果为true则代表选中
    boy_redio.checked = true;//赋值

</script>
```

> 提交表单，MD5加密密码，表单优化

```html
<script src="https://cdn.bootcss.com/blueimp-md5/2.10.0/js/md5.min.js"></script>
<form action="#" method="post" onsubmit="aaa()">
    <p>
        <span>用户名：</span> <input type="text" id="username" name="username">
    </p>

    <p>
      <span>密码</span> <input type="password" id="password" >
    </p>
    <input type="hidden" id="md5_password" name="password">
<!--绑定事件  onclick 被点击   -->
    <button type="submit" onclick="aaa()">提交</button>
</form>

<script>
    function aaa() {
        var uname = document.getElementById('username');
        var psd = document.getElementById('password');
        var md5_psd = document.getElementById('md5_password');
        console.log(uname.value);
        psd.value = md5(psd.value);
        //md5_psd.value = md5(psd.value);
       //MD5算法 可以校验判断表单，true就是通过提交，false就是阻止提交
        return true;

    }
</script>
```

# 10、jQuery

jQuery库，里面存在大量的javascript函数

> 获取jquery

- 可以导入jQuery在线引用地址

例如< script src=“http://libs.baidu.com/jquery/2.1.4/jquery.min.js”>< /script >

- 官网下载https://jquery.com/download/ 导入项目中

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Title</title>
    <script src="lib/jquery-3.6.0.min.js"></script>

</head>
<body>
<a href="#" id="text_jquery">点我</a>
<script>
    //公式：$(selector).action()
    //选择器就是CSS的选择器
    $('#text_jquery').click(function () {
        alert('1111');
    })
</script>

</body>
</html>
```

> 选择器

```HTML
<script>
    //原生js，选择器少，麻烦不好记
    //标签
    document.getElementsByTagName();
    //id
    document.getElementById();
    //类
    document.getElementsByClassName();

    //jQuery选择器 css里的选择器都能用
    $('p').click(); //标签选择器
    $('.class').click();//类选择器
    $('#id').click();//id选择器

</script>
```

文档工具站：https://jquery.cuishifeng.cn/

> 事件

- 鼠标事件、键盘事件、其他事件

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Title</title>
    <script src="lib/jquery-3.6.0.min.js"></script>
    <style>
        #divMove{
            width: 500px;
            height: 500px;
            border: 1px solid red;
        }
    </style>
</head>
<body>
<!--要求：获取鼠标当前的一个坐标-->
mouse:<span id="mouseMove"></span>
<div id="divMove">
    在这里移动鼠标试试
</div>

<script>
    //当网页元素加载完毕后，响应事件
    $(function (){
        $('#divMove').mousemove(function (e){
            $('#mouseMove').text('x:'+e.pageX + '  y'+e.pageY)
        });
    });
</script>
</body>
</html>
```

> 操作Dom

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="js/jquery-3.5.1.js"></script>
</head>
<body>
<ul id="test-ul">
    <li id="js">JavaScript</li>
    <li name="JavaSE">JavaSE</li>
</ul>

<script>
    // 获得值
    $('#test-ul li[name=JavaSE]').text();
    // 设置值
    $('#test-ul li[name=JavaSE]').text('Python');

    // css 的操作
    $('#test-ul li[name=JavaSE]').css('color','red');

    // 元素的显示
    $('#test-ul li[name=JavaSE]').show();

    // 元素的隐藏
    $('#test-ul li[name=JavaSE]').hide();
</script>
</body>
</html>

<script>
// 获得值
$('#test-ul li[name=JavaSE]').text();
// 设置值
$('#test-ul li[name=JavaSE]').text('Python');

// css 的操作
$('#test-ul li[name=JavaSE]').css('color','red');

// 元素的显示
$('#test-ul li[name=JavaSE]').show();

// 元素的隐藏
$('#test-ul li[name=JavaSE]').hide();

</script>
</body>
</html>
```

