---
title: 在idea中使用Tomact遇到的问题
date: 2021-09-10 11:37:12
tags: idea
---

一个奇奇怪怪的问题，至今没搞明白原因...

<!-- more -->

在idea中不使用maven模板创建一个javaweb项目，在配置好tomcat之后出现的一个问题，服务器正常启动，但在访问特定路径时浏览器报错500，但代码没有出错，摸索半天没有解决。

在删除了.iml 里的

```java
<component name="NewModuleRootManager" inherit-compiler-output="true">
  <exclude-output />
  <content url="file://$MODULE_DIR$" />
  <orderEntry type="inheritedJdk" />
  <orderEntry type="sourceFolder" forTests="false" />
</component>
```

重启tomact 问题得到解决，不知道原理是什么，先记录一下。

