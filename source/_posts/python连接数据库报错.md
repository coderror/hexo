---
title: Python3出现"No module named 'MySQLdb'"问题
date: 2021-12-22 15:09:37
tags: [python,mysql]
---

在python3.x中使用sqlalchemy连接数据库，报错"No module named 'MySQLdb' “，的解决办法。

<!-- more -->





**Python3 与 sqlalchemy**连接数据库，出现了报错： **No module named 'MySQLdb'**。原因如下：

1. 在 python2 中，没有安装mysqldb的库
2. 在 python3 中，改变了连接库，改为了 pymysql 库，从而使mysqldb不再支持python3

解决办法如下：

1. 如果是在python2.x中，那么我们只需检查自己是否已经安装了mysqldb的连接库，在终端中输入 pip list 查看已经安装的库里是否有mysqldb。

   ![image-20211222152341373](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211222152341373.png)

   如果没有就 输入 pip install mysqldb 安装，然后再项目中 import mysqldb 使用即可。

2. 如果是在python3.x中，因为mysqldb已经不支持python3了，而且把连接库换成了pymysql，这时我们只需 pip install pymysql 下载库，然后在项目中 import pymysql 就可以使用了。

> 要注意以下问题，需修改相关的代码

在使用sqlalchemy连接数据库时，我们的代码是：

```python
from sqlalchemy import create_engine

    engine = create_engine('mysql+mysqldb://root:xxx@localhost:3306/weatherdata?charset=utf8')
    total = pd.read_csv('你的cvs名称')
    total.to_sql(con=engine, name='hg2021', if_exists='replace', index=False)
```

在python3.x的环境下需修改为  **mysql+pymysql**

```python
from sqlalchemy import create_engine
import pymysql
    engine = create_engine('mysql+pymysql://root:xxx@localhost:3306/weatherdata?charset=utf8')
    total = pd.read_csv('你的cvs名称')
    total.to_sql(con=engine, name='hg2021', if_exists='replace', index=False)
```

---



**一些奇怪的问题**：博主自己使用的python3.9 在项目中使用sqlalchemy连接数据库，使用了mysqldb，并没有报错No module named 'MySQLdb'，而且数据正常的导入到了数据库中，在帮室友调试时，他们的环境同样也是python3，却会报错，推测应该与pycharm的版本有关系，我的版本为2020.3.3，室友的都是2021年的版本。
