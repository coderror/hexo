---

title: 【教程】Gitee图床问题的解决办法
date: 2022-03-29 08:33:50
tags: [java,教程，MarkDown]
---

教你如何购买配置阿里云对象存储OSS、配置Picgo、配置Typora以及替换文章中原来图片的链接，以用来解决Gitee图床gg的问题。

<!-- more -->

# 事情的起因

就在前天我躺在床上刷着手机，偶然间刷到一篇鱼皮的文章，标题写着“**突发！Gitee图床，废了**” 因为我自己就是用的Gitee当做图床，所以我就点进去的看了看，对文章内容半信半疑的我赶忙打开了自己的博客，好家伙Gitee简直不讲武德，图片全没了。

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329084432085.png" alt="image-20220329084432085" style="zoom:67%;" />

# 我的看法

我文章的所有图片都存储在Gitee上，连本地都没有备份，想到这几百张图片的链接替换我就一阵头疼，Gitee真是给我上了生动的一课，**免费的才是最贵的**。不过好在我的外链不多，Gitee没有封禁我的图床仓库。于是我立马去Gitee把我的图片都down了下来。

虽然我知道咱白嫖Gitee的资源咱理亏，但也请Gitee以后不要标榜自己是中国的Github了！！！

# 解决办法

1. 使用云存储做替代，付费！
2. 用脚本读取我的所有文章替换文章中原来的图片链接。

好了废话不多说，下面是详细教程。

# 阿里云对象存储OSS

## 1.购买

首先你得有个阿里云账号，搜索对象存储。![QQ图片20220329090205](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/QQ%E5%9B%BE%E7%89%8720220329090205.png)

然后按照你自己的需求购买相应的容量，我只用来放博客图片所以40G很够用了，买它个两三年反正也没多少钱。![QQ图片20220329090415](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/QQ%E5%9B%BE%E7%89%8720220329090415.png)

购买完毕。

注意：购买完存储包就可以了，至于流量包，默认的是按量计费，下图显示的是阿里云的计费方法：

![441](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/441.png)

## 2.配置Bucket

购买完成后，继续进行创建 Bucket工作。![QQ图片20220329090503](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/QQ%E5%9B%BE%E7%89%8720220329090503.png)

读写权限要设定为公共读，其他的服务选项，如果自己有特殊需要，可以选择配置同城冗余存储，版本控制等服务，如果没有可以默认。

![image-20220329090729419](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329090729419.png)

完成后点击确定即可。

## 3.创建用户设置访问控制

在我的中找到访问控制选项。

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329091752400.png" alt="image-20220329091752400" style="zoom:67%;" />

选择用户后，点击创建用户。

![H0AV$0PWHGKC4ET01SBD686](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/H0AV$0PWHGKC4ET01SBD686.png)

设置一个你喜欢的名称，选择通过API调用访问。

![7878](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/7878.png)

点击确认后我们就会跳转到这个界面。

![image-20220329092158225](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329092158225.png)

请保存好你的accesskey，因为这个页面只会出现一次，复制一下保存好，后面配置Picgo会用到。

在访问控制中找到权限管理<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/555.png" alt="555" style="zoom:80%;" />

选择授权主体后，授予它管理对象存储服务OSS的权限

![55511](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/55511.png)

OK，对象存储OSS配置完成。

# Picgo配置

默认你会使用Picgo，不再细讲了。

![555111](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/555111.png)

这里就用到了我们上面用户的相关信息，如果你不知道存储区域可以从这里看。

![](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/5511.png)

![](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/511.png)

OK了，Picgo也配置完成了。

# 文章图片链接替换

解决办法：

1. 一张一张的修改链接（这样太呆了）
2. 在typora中使用Ctrl+F，查找原链接进行替换。
3. 编写代码读取所有的文章进行链接替换。（最优）



首先我们对比一下图片在Gitee上的链接和在阿里云上的链接

![image-20220329093446213](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329093446213.png)

由此我们可以发现我们只需替换图片名称之前的前缀，这样会方便许多，当然这样做的前提是你上传在Gitee的图片和上传在阿里云对象存储的图片的名称是一致的。

下面是java的实现代码：（记得修改<>的内容）

```java
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @ClassName ChangePicPath
 * @Description 修改指定文件夹中的所有信息
 * @Author faro_z
 * @Date 2022/3/26 5:18 下午
 * @Version 1.0
 **/
public class ChangePicPath {
    private static String folderName = "<你的文章所在的文件夹的全路径名>";
    private static String from = "<图片访问路径中待替换的部分>";
    private static String to = "<图片访问路径中被替换成的部分>";
    private static List<String> errFileNameList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        List<String> pathList = getAllFileName(folderName);
        System.out.println("成功获取所有文件名称！");
        System.out.println("第一个文件名称为:"+pathList.get(0));
        // 如果发现文件名称和预期的不一样，别误操作了
        System.out.println("输入任意数字，再按回车键继续...");
        new Scanner(System.in).nextInt();
        int errCount = 0;
        for (String path : pathList) {
            if (!changeFileContent(from,to,path)) {
                errCount++;
            }
        }
        System.out.println("出错文件数为："+errCount);
        System.out.println("所有出错文件名为:");
        for (String errPath : errFileNameList) {
            System.out.println(errPath);
        }
    }

    /**
     * 获取当前文件夹下所有文件名（不包括文件夹名）
     * @param folderName
     * @return
     */
    private static List<String> getAllFileName(String folderName) {
        ArrayList<String> filePathList = new ArrayList<>();
        dfs(folderName,filePathList);
        return filePathList;
    }

    /**
     * 递归获取文件名
     * @param path
     * @param filePathList
     */
    private static void dfs(String path, List<String>filePathList) {
        File file = new File(path);
        if (file.isFile() && file.getName().endsWith(".md")) {
            filePathList.add(file.getAbsolutePath());
            return;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File tmpFile : files) {
                dfs(tmpFile.getAbsolutePath(),filePathList);
            }
        }
    }

    private static boolean changeFileContent(String from,String to,String filePath) {
        File file = new File(filePath);
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            CharArrayWriter tempStream = new CharArrayWriter();

            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                line = line.replaceAll(from,to);
                tempStream.write(line);
                tempStream.append(System.getProperty("line.separator"));
            }

            bufferedReader.close();
            // 将内存中的流 写入 文件
            FileWriter out = new FileWriter(file);
            tempStream.writeTo(out);
            out.close();
        } catch (IOException e) {
            errFileNameList.add(filePath);
            return false;
        }
        return true;
    }
}
```

实现效果：![882](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/882.png)

**如果你能够看见这篇博客的图片，那我想这就是最好的实现效果。**

# 配置Typora

在Typora 中找到偏好设置，选择图像，进行下图操作。

![image-20220329094727544](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220329094727544.png)

----

----

[参考文章](https://blog.csdn.net/weixin_44062380/article/details/123761358)
