---
title: 【教程】Fastgithub的下载及使用
date: 2022-01-21 23:24:37
tags: [github,教程]
---

你还在因不能访Github而烦恼吗？这篇文章介绍了一个强大的工具，让你优雅的访问Github。

<!-- more -->

## GitHub

GitHub作为全世界最大的同性交友平台，也是开源世界中的王者，上面有许许多多免费的资源，但是国内由于一些原因导致github访问速度很慢甚至访问不了，原因有很多，比如政治方面，但最直接最主要的原因是DNS污染。

> DNS污染：网域服务器缓存污染（DNS cache pollution），又称域名服务器缓存投毒（DNS cache poisoning），是指一些刻意制造或无意中制造出来的域名服务器数据包，把域名指往不正确的IP地址。一般来说，在互联网上都有可信赖的网域服务器，但为减低网络上的流量压力，一般的域名服务器都会把从上游的域名服务器获得的解析记录暂存起来，待下次有其他机器要求解析域名时，可以立即提供服务。一旦有关网域的局域域名服务器的缓存受到污染，就会把网域内的计算机导引往错误的服务器或服务器的网址。

所以经常可以看见网上有一些访问github的方法是修改Host，通过修改DNS解析，直接访问Github的CDN节点，但是我亲测这个方法可行性很低，看运气。由于我家的网络是移动的，更是墙中墙，所以没有成功过。下面我推荐我最近发现的一个工具，亲测可以让你畅通无阻的访问GitHub，从此再也见不到 “无法访问”。

## **FastGithub**

> FastGithub是一个开源的软件主要为了使GitHub畅通无阻，有超大量的IP资源、快速的IP检测功能，以及微小的宽带占用，智能的DNS设置，并且支持多平台和加速GitHub的其它域名

**简单的说FastGithub就是github定制版的dns服务，解析访问github最快的ip。**

![image-20220121234612129](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220121234612129.png)

### 加速原理

- 使用github公开的ip范围，扫描所有可用的ip；
- 间隔指定时间(5min)检测与记录扫描到的ip的访问耗时；
- 拦截dns，访问github时，返回最快的ip；

### FastGithub下载

[GitHub地址](https://github.com/dotnetcore/FastGithub )先甩这里，等你能正常访问GItHub了可用去下载最新版。

我找到了一个Windows版本的镜像 [清华镜像](https://cloud.tsinghua.edu.cn/d/df482a15afb64dfeaff8/files/?p=%2Ffastgithub_win-x64.zip)

![image-20220121235659362](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220121235659362.png)

下载完成后解压，在文件夹中找到.exe程序，这两个都可。

![image-20220121235911859](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220121235911859.png)然后以管理员的身份运行cmd，输入运行FastGithub的命令即可运行 

**E:\fastgithub_win-x64\fastgithub_win-x64**是你将fast GitHub下载存放的位置

```text
E:\fastgithub_win-x64\fastgithub_win-x64\FastGithub.exe start    #启动fast GitHub

E:\fastgithub_win-x64\fastgithub_win-x64\FastGithub.exe stop     #停止fast GitHub
```

服务启动后就可以流畅的访问GitHub了。

![image-20220122000846027](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220122000846027.png)

**注意：每次访问前要保证FastGithub已经启动了**





---

## **参考文章**

[Fastgithub下载及使用](https://zhuanlan.zhihu.com/p/428454772)

