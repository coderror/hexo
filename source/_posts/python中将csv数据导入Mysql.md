---
title: python中将csv数据导入Mysql
date: 2021-12-09 17:17:20
tags: python
---

在做大数据期末作业时遇到的问题，如何在python中连接mysql，以及如何将csv格式数据保存在数据库中。

<!-- more -->

pandas的DataFrame数据，使用

```python
df.to_sql接口时，
from sqlalchemy import create_engine
```

使用SQLAlcheymy的ORM框架，链接数据库

```sql
engine = create_engine('mysql+mysqldb://root:***@localhost:3306/weatherdata?charset=utf8')
total.to_sql(con=engine, name='HG2021', if_exists='replace', index=False)
```

报错

```python
sqlalchemy.exc.InvalidRequestError: Could not reflect: requested table(s) not available in Engine(mysql+mysqldb://root:***@localhost:3306/weatherdata?charset=utf8): (HG2021)
```

起初以为是数据库中表的格式问题，重新创建表后还是报错，后又更换了数据库的连接方式，还是报错

后来发现是表名大小写问题，表名大写就会出现报错，小写就可以正常创建数据库。

```python
engine = create_engine('mysql+mysqldb://root:***@localhost:3306/weatherdata?charset=utf8')
    total.to_sql(con=engine, name='hg2021', if_exists='replace', index=False)
```



真的是给我整吐了，因为一个大小写搞了一下午。
