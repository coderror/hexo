---
title: 【教程】MySQL安装与配置保姆级教程！
date: 2021-11-16 16:37:08
tags: [mysql,教程]
---

MySQL是一种关系数据库管理系统，所使用的 SQL 语言是用于访问数据库的最常用的，本文详细叙述了mysql数据库免安装版的下载与配置流程。

保姆级教程

<!-- more -->

# 1.免安装版的MySQL

> MySQL是一种关系数据库管理系统，所使用的 SQL 语言是用于访问[数据库](https://baike.baidu.com/item/数据库/103728)的最常用的标准化语言，其特点为体积小、速度快、总体拥有成本低，尤其是[开放源码](https://baike.baidu.com/item/开放源码/7176422)这一特点，在 Web应用方面 MySQL 是最好的 RDBMS(Relational Database Management System：关系数据库管理系统)应用软件之一。



 第一步：要先进入mysql官网里（Mysql的官网-->https://www.mysql.com/），

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219170000269.png" alt="image-20211219170000269" style="zoom:67%;" />

第二步：点击后进入跳转的页面，拉倒最下方

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219170136477.png" alt="image-20211219170136477" style="zoom: 50%;" />

第三步：红色框框出的就是免费的社区版，下载社区版。

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219170319559.png" alt="image-20211219170319559" style="zoom:50%;" />

第四步：下载Windows版 如下图所示

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219170552208.png" alt="image-20211219170552208" style="zoom:67%;" />

第五步：不登录直接下载

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219170746033.png" alt="image-20211219170746033" style="zoom:67%;" />

**注意这里安装包的路径不要包含中文，尽量下载在你找的到的目录，路径应尽量简短,避免配置时找不到路径。（我的路径）**

```java
  D:\MySQL
```

![image-20211219172121288](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219172121288.png)

# 2.MySQL的配置

　第一步：**以管理员身份打开命令行(如下图所示)，一定要是管理员身份，否则由于后续部分命令需要权限，出现错误！**

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211222153556228.png" alt="image-20211222153556228" style="zoom:67%;" />

第二步：下转到mysql的bin目录下：

> cd进入你自己的mysql下的bin目录的路径 （每个人的路径是不同的）

![img](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/2021062414542192.jpg)

第三步：安装mysql的服务：

```
命令：mysqld --install
```



![img](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/2021062414542193.jpg)

> 在帮别人安装时出现过这种情况：

![在这里插入图片描述](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/20200620153518599.png)

根据提示，将**mysqld**更改为   **.\mysqld**  即可。

第四步：初始化mysql

  ```cmd
  在这里，**初始化会产生一个随机密码**,如下图框框所示，**记住这个密码**，后面会用到
  命令：mysqld --initialize --console
  ```



![img](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/2021062414542194.jpg)

第五步：开启mysql的服务

```
命令：net start mysql
```


![img](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/2021062414542195.jpg)

第六歩：登录验证

​		mysql是否安装成功！(要注意上面产生的随机密码，不包括前面符号前面的空格，否则会登陆失败)，如果和下图所示一样，则说明你的mysql已经安装成功！注意，，一定要先开启服务，不然会登陆失败，出现拒绝访问的提示符！！！

```
命令：mysql -u root -p
输入之前产生的随机密码
```

![img](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/2021062414542196.jpg)

第七歩：修改密码

​		初始化产生的随机密码太复杂，我们改一个简单的。**（请牢记你修改后的密码）**

![image-20211219181021131](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219181021131.png)

```cmd
命令：alter user root@localhost identified by '你的密码'
```

> 如果密码修改失败，请先完成第八步，再完成第七歩.

第八步：设置系统的全局变量

点击"我的电脑"-->"属性"-->''高级系统设置''-->''环境变量'',接下来如下图所操作

因为我的系统是Windows11所以步骤有点不一样。

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219180144382.png" alt="image-20211219180144382" style="zoom:67%;" />

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219180256472.png" alt="image-20211219180256472" style="zoom:67%;" />

新建一个系统变量

```
变量名：mysql
变量值：你的mysql路径
```

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/1727568-20190915195658938-215279743.jpg" alt="img" style="zoom:67%;" />

把新建的mysql变量添加到Path路径变量中，点击确定，即完成

<img src="https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/1727568-20190915195808186-1920486822.jpg" alt="img" style="zoom:80%;" />

```cmd
在新建中添加 %mysql%\bin
```



配置完成之后，每当我们想要用命令行使用mysql时，只需要win+R，-->输入"cmd"打开命令行，之后输入登录sql语句即可。

至此，一个免安装版的Mysql就安装并配置完成了。





---



# 3.密码修改的另一种办法

![image-20211219175514044](https://lkb-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20211219175514044.png)

```cmd
命令：set password for root@localhost = password('你要改的密码');
提示：上面这条命令因为在mysql环境中所以带有 ; （分号）必须带上
exit退出mysql后再登录。
```

